import { useCartStore } from "@/store/index";
import { onShow } from "@dcloudio/uni-app";

export default () => {
    onShow(() => {
        uni.setTabBarBadge({
            index: 2,
            text: useCartStore().total + "",
        });
    });
};
