import { defineStore } from "pinia";
import { ref, computed } from "vue";

export const useUserStore = defineStore("user", () => {
    //登陆
    const token = ref(uni.getStorageSync("token") || "");
    //用户信息
    const userInfo = ref(JSON.parse(uni.getStorageSync("userInfo") || "{}"));
    //地址对象
    const address = ref<Address>(JSON.parse(uni.getStorageSync("address") || "{}"));
    //地址拼接
    const addstr = computed(() => {
        if (!address.value.provinceName) return "";
        const { provinceName, cityName, countyName, detailInfo } = address.value;
        return "" + provinceName + cityName + countyName + detailInfo;
    });
    //更新地址对象
    function updateAddress(add: Address) {
        address.value = add;
        uni.setStorageSync("address", JSON.stringify(address.value));
    }
    //更新用户信息
    function updateUserInfo(info: any) {
        userInfo.value = info;
        uni.setStorageSync("userInfo", JSON.stringify(userInfo.value));
    }
    //更新token
    function updateToken(value: string) {
        token.value = value;
        uni.setStorageSync("token", value);
    }
    //退出登陆
    function logOut() {
        uni.showModal({
            title: "提示",
            content: "确认退出登陆吗?",
            success: (res) => {
                if (res.cancel) return;
                updateUserInfo({});
                updateAddress({});
                updateToken("");
            },
        });
    }
    return { token, userInfo, address, addstr, updateToken, updateAddress, updateUserInfo, logOut };
    interface Address {
        cityName?: string;
        countyName?: string;
        detailInfo?: string;
        detailInfoNew?: string;
        errMsg?: string;
        nationalCode?: string;
        nationalCodeFull?: string;
        postalCode?: string;
        provinceName?: string;
        streetName?: string;
        telNumber?: string;
        userName?: string;
    }
});
export const useCartStore = defineStore("cart", () => {
    //购物车数组
    const cart = ref<Cart[]>(JSON.parse(uni.getStorageSync("cart") || "[]"));
    //商品数量
    const total = computed(() => {
        return cart.value.reduce((num, goods) => (num += goods.goods_count), 0) || 0;
    });
    //选中商品的数量
    const radioTotal = computed(() => cart.value.filter((goods) => goods.goods_state).reduce((num, goods) => (num += goods.goods_count), 0) || 0);
    //选中商品的价格
    const price = computed(
        () =>
            cart.value
                .filter((goods) => goods.goods_state)
                .reduce((num, goods) => (num += goods.goods_price * goods.goods_count), 0)
                .toFixed(2) || "0.00"
    );
    //添加商品
    function addToCart(goods: Cart) {
        const find = cart.value.find((fGoods) => fGoods.goods_id === goods.goods_id);
        console.log();
        if (!find) {
            cart.value.push(goods);
        } else {
            find.goods_count++;
        }
        saveCartStorage();
    }
    //更改所有勾选状态
    function updateAllGoodsState(state: boolean) {
        cart.value.forEach((goods) => (goods.goods_state = !state));
        saveCartStorage();
    }
    //更改勾选状态
    function updateGoodsState(id: number, state: boolean) {
        const find = cart.value.find((goods) => goods.goods_id === id);
        if (find) {
            find.goods_state = state;
            saveCartStorage();
        }
    }
    //更改商品数量
    function updateGoodsCount(id: number, count: number) {
        const find = cart.value.find((goods) => goods.goods_id === id);
        if (find) {
            find.goods_count = count;
            saveCartStorage();
        }
    }
    //删除商品
    function delGood(id: number) {
        cart.value = cart.value.filter((goods) => goods.goods_id !== id);
        saveCartStorage();
    }
    //缓存购物车数据
    function saveCartStorage() {
        uni.setStorageSync("cart", JSON.stringify(cart.value));
    }
    return { cart, total, radioTotal, price, addToCart, updateGoodsState, updateAllGoodsState, updateGoodsCount, delGood };

    interface Cart {
        goods_id: number;
        goods_name: string;
        goods_price: number;
        goods_count: number;
        goods_small_logo: string;
        goods_state: boolean;
    }
});
